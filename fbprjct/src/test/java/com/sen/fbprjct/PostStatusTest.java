
package com.sen.fbprjct;

import com.sen.fbprjct.pages.HomePage;
import org.testng.annotations.Test;

public class PostStatusTest extends BaseTests{
    
    /* 
    * This test does not assert anything, it just automates the status post.
    */
    @Test
    public void testPostStatus() {
        // Enter test data below
        String username = ""; // Enter facebook username
        String password = ""; // Enter facebook password
        // Enter test data above
        String statusMessage = "Hello World";
        HomePage homePage = loginPage.loginToFacebook(username, password);
        homePage.postStatus(statusMessage);
    }
}
