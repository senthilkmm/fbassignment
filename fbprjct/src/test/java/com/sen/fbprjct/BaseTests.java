package com.sen.fbprjct;

import com.sen.fbprjct.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class BaseTests {
    
    private final String driverKey = "webdriver.gecko.driver"; // firefox driver property key
    private final String driverPath = "resources/geckodriver.exe"; // firefox driver exe location
    private final String url = "https://www.facebook.com/";

    private WebDriver driver;
    
    protected LoginPage loginPage;
    
    @BeforeClass
    public void setup() throws InterruptedException {
        System.setProperty(driverKey, driverPath);
        driver = new FirefoxDriver();
    }
    
    @BeforeMethod
    public void launchFacebookLogin() {
        driver.get(url);
        loginPage = new LoginPage(driver);
    }
    
    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
