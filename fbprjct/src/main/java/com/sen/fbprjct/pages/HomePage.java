package com.sen.fbprjct.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    private final WebDriver driver;
    
    private final By statusUpdateTextArea = By.name("xhpc_message");
    private final By postButton = By.xpath("//button[contains(.,'Post')]");
    
    // do not make the below string final, string substitution happens later
    private String postedMessageXpathStr = "//*[contains(@class,\'_5_jv\')]/p[contains(text(),\'%s\')]";
    
    private WebDriverWait wait;
    private final long timeOutInSeconds = 20;
    
    public HomePage(WebDriver driver){
        this.driver = driver;
    }
    
    public void postStatus(String statusMessage) {
        wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(ExpectedConditions.presenceOfElementLocated(statusUpdateTextArea)).sendKeys(statusMessage);
        wait.until(ExpectedConditions.elementToBeClickable(postButton)).click();
        String formattedPostedMessageStr = String.format(postedMessageXpathStr, statusMessage);
        By postedMessage = By.xpath(formattedPostedMessageStr);
        wait.until(ExpectedConditions.presenceOfElementLocated(postedMessage)); // ensures the status is posted on the wall
    }
}
