
package com.sen.fbprjct.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    private final WebDriver driver;
    
    private final By emailField = By.id("email");
    private final By passwordField = By.id("pass");
    
    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }
    
    public HomePage loginToFacebook(String username, String password) {
        driver.findElement(emailField).sendKeys(username);
        driver.findElement(passwordField).sendKeys(password + Keys.RETURN);
        return new HomePage(driver);
    }
}
